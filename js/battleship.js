/**
 * Created by Eric Ohtake on 6/11/2016.
 */

var view = {
    displayMessage: function (msg) {
        var messageArea = document.getElementById("messageArea");
        messageArea.innerHTML = msg;
    },

    displayHit: function (location) {
        var cell = document.getElementById(location);
        cell.setAttribute("class", "hit");
    },

    displayMiss: function (location) {
        var cell = document.getElementById(location);
        cell.setAttribute("class", "miss");
    }
};

var model = {
    boardSize: 7,
    numShips: 3,
    shipLength: 3,
    shipsSunk: 0,
    // The ships array has a location and hits array inside. Hard coded for now
    ships: [{ locations: ["0", "0", "0"], hits: ["", "", ""] },
            { locations: ["0", "0", "0"], hits: ["", "", ""] },
            { locations: ["0", "0", "0"], hits: ["", "", ""] }],

    // This method take a location as string 'guess' and checks if there is a ship on that position. If there is, return
    // true and register a hit. If there isn't, the method returns false.
    fire: function (guess) {
        // It will run the code on the ships available in the numShips property.
        for (var i = 0; i < this.numShips; i++) {
            // It will take the index of the ship number and set the right ship of the ships array.
            var ship = this.ships[i];
            // And create an index, looking into the ship.location array based on the ship position of the guess.
            var index = ship.locations.indexOf(guess);
            // If the index is greater or equal 0, it means that there is a ship in the given location, and it is a hit.
            if (index >= 0) {
                // It uses the variable ship and its position based in the index of the guess, and change to "hit".
                ship.hits[index] = "hit";
                view.displayHit(guess);
                view.displayMessage("HIT!");
                // We call the helper isSunk method to determine if a ship has been sunk, and keep track of how many.
                if (this.isSunk(ship)) {
                    view.displayMessage("You sank a ship!");
                    this.shipsSunk++;
                }
                return true;
            }
        }
        view.displayMiss(guess);
        view.displayMessage("You missed!");
        return false;
    },

    // This method checks if a ship passed as argument has a hit on its array. Returns false if there isn't.
    isSunk: function(ship) {
        for (var i = 0; i < this.shipLength; i++) {
            if (ship.hits[i] !== "hit") {
                return false;
            }
        }
        return true;
    },

    collision: function (locations) {
        for (var i = 0; i < this.numShips; i++) {
            var ship = model.ships[i];
            for (var j = 0; j < locations.length; j++) {
                if (ship.locations.indexOf(locations[j]) >= 0) {
                    return true;
                }

            }
        }
        return false;
    },

    generateShipLocations: function () {
        var locations;
        for (var i = 0; i < this.numShips; i++) {
            do {
                locations = this.generateShip();
            } while (this.collision(locations));
            this.ships[i].locations = locations;
        }
    },

    generateShip: function () {
        var direction = Math.floor(Math.random() * 2);
        var row, col;

        if (direction === 1) {
            row = Math.floor(Math.random() * this.boardSize);
            col = Math.floor(Math.random() * (this.boardSize - this.shipLength));
        } else {
            row = Math.floor(Math.random() * (this.boardSize -this.shipLength));
            col = Math.floor(Math.random() * this.boardSize);
        }

        var newShipLocations = [];
        for (var i = 0; i < this.shipLength; i++) {
            if (direction === 1) {
                newShipLocations.push(row + "" + (col + i));
            } else {
                newShipLocations.push((row + i) + "" + col);
            }
        }
        return newShipLocations;
    },


};

var controller = {
    guesses: 0,

    processGuess: function(guess) {
      var location = parseGuess(guess);
        if(location) {
            this.guesses++;
            var hit = model.fire(location);
            if (hit && model.shipsSunk === model.numShips) {
                view.displayMessage("You sank all my battleships, in " + this.guesses + " guesses.");
            }
        }
    },

};

function parseGuess(guess) {
    var alphabet = ["A", "B", "C", "D", "E", "F", "G"];

    if (guess === null || guess.length !== 2) {
        alert("I can't accept this input. Look at the board and guess right!");
    } else {
        var firstChar = guess.charAt(0);
        var row = alphabet.indexOf(firstChar);
        var column = guess.charAt(1);

        if (isNaN(row) || isNaN(column)) {
            alert("Ops! This is not in the board. Please, guess right!");
        } else if (row < 0 || row >= model.boardSize || column < 0 || column >= model.boardSize) {
            alert("Ops! That's off the board! Please guess right!");
        } else {
            return row + column;
        }
    }
    return null;
};

function handleFireButton() {
    var guessInput = document.getElementById("guessInput");
    var guess = guessInput.value;
    controller.processGuess(guess);
    guessInput.value = "";
}

function handleKeyPress(e) {
    var fireButton = document.getElementById("fireButton");
    if (e.keyCode === 13) {
        fireButton.click();
        return false;
    }
}

function init() {
    var fireButton = document.getElementById("fireButton");
    fireButton.onclick = handleFireButton;
    var guessInput = document.getElementById("guessInput");
    guessInput.onkeydown = handleKeyPress;
    model.generateShipLocations();
}

window.onload = init;

// Simple test to check if the model items are accessible and displayed using the view object.
/*view.displayMessage(model.ships[0].locations[2] + " " + model.ships[0].hits[0]);*/

// Simple test to check the logic between the view and the model.
/*controller.processGuess("A0");
controller.processGuess("A6");
controller.processGuess("B6");
controller.processGuess("C6");
controller.processGuess("C4");
controller.processGuess("D4");
controller.processGuess("E4");
controller.processGuess("B0");
controller.processGuess("B1");
controller.processGuess("B2");*/
